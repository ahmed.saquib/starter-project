package net.spectrum.api.security.menu.repository;

import java.util.Optional;
import net.spectrum.api.security.menu.entity.MenuEntity;
import net.spectrum.api.security.privilege.entity.PrivilegeEntity;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Menu entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MenuRepository extends JpaRepository<MenuEntity, String> {
    @NotFound(action = NotFoundAction.IGNORE)
    Optional<MenuEntity> findByOid(String oid);
}