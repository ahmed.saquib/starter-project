package net.spectrum.api.security.privilege.controller;

import java.util.List;
import net.spectrum.api.security.privilege.entity.PrivilegeEntity;
import net.spectrum.api.security.privilege.repository.PrivilegeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/privilege")
public class PrivilegeController {

    @Autowired
    PrivilegeRepository privilegeRepository;

    @GetMapping("")
    public ResponseEntity<List<PrivilegeEntity>> getPrivilegeObjects(){
         return ResponseEntity.ok(privilegeRepository.findAll());
    }
}
