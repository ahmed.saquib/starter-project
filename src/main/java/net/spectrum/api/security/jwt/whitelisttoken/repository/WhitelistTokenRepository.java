package net.spectrum.api.security.jwt.whitelisttoken.repository;

import java.util.List;
import javax.annotation.Resource;
import net.spectrum.api.security.jwt.whitelisttoken.entity.WhitelistTokenEntity;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class WhitelistTokenRepository {
    
    public static final String HASH_KEY = "WhitelistToken";

    @Resource(name = "redisTemplate")
    private RedisTemplate<String, ?> template;

    public WhitelistTokenEntity save(WhitelistTokenEntity blacklistToken){
        template.opsForHash().putIfAbsent(HASH_KEY, blacklistToken.getToken(), blacklistToken);
        return blacklistToken;
    }

    public List<Object> findAll(){
        return template.opsForHash().values(HASH_KEY);
    }

    public Boolean findByToken(String token){
        Object whitelistToken = template.opsForHash().get(HASH_KEY, token);
        return whitelistToken != null;
    }

    public String deleteToken(String token){
        template.opsForHash().delete(HASH_KEY, token);
        return "Token removed";
    }
}
