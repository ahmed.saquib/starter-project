package net.spectrum.api.security.jwt.blacklisttoken.repository;


import java.util.List;
import javax.annotation.Resource;
import net.spectrum.api.security.jwt.blacklisttoken.entity.BlacklistTokenEntity;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class BlacklistTokenRepository {

    public static final String HASH_KEY = "BlacklistToken";

    @Resource(name = "redisTemplate")
    private RedisTemplate<String,?> template;

    public BlacklistTokenEntity save(BlacklistTokenEntity blacklistToken){
        template.opsForHash().putIfAbsent(HASH_KEY, blacklistToken.getToken(), blacklistToken);
        return blacklistToken;
    }

    public List<Object> findAll(){
        return template.opsForHash().values(HASH_KEY);
    }

    public Boolean findByToken(String token){
       Object blacklistToken = template.opsForHash().get(HASH_KEY, token);
       return blacklistToken != null;
    }

    public String deleteToken(String token){
        template.opsForHash().delete(HASH_KEY, token);
        return "Token removed";
    }
}
