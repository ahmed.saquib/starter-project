package net.spectrum.api.security.jwt.utils;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import net.spectrum.api.security.jwt.blacklisttoken.entity.BlacklistTokenEntity;
import net.spectrum.api.security.jwt.blacklisttoken.repository.BlacklistTokenRepository;
import net.spectrum.api.security.jwt.whitelisttoken.entity.WhitelistTokenEntity;
import net.spectrum.api.security.jwt.whitelisttoken.repository.WhitelistTokenRepository;
import net.spectrum.api.security.userdetails.entity.UserDetailsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import io.jsonwebtoken.*;
import org.springframework.util.StringUtils;


//  This class has 3 main functions:
//
//    getJwtFromCookies: get JWT from Cookies by Cookie name
//    generateJwtCookie: generate a Cookie containing JWT from username, date, expiration, secret
//    getCleanJwtCookie: return Cookie with null value (used for clean Cookie)
//    getUserNameFromJwtToken: get username from JWT
//    validateJwtToken: validate a JWT with a secret
@Component
public class JwtUtils {

    private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

    @Value("${jwt.secret}")
    private String jwtSecret;

    @Value("${jwt.expiration-in-ms}")
    private int jwtExpirationMs;
    @Autowired
    BlacklistTokenRepository blacklistTokenRepository;

    @Autowired
    WhitelistTokenRepository whitelistTokenRepository;

    public String generateJwtToken(Authentication authentication) {

        UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();

        return Jwts.builder().setSubject((userPrincipal.getUsername())).setIssuedAt(new Date())
            .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
            .signWith(SignatureAlgorithm.HS512, jwtSecret).compact();
    }

    public String getUserNameFromJwtToken(String token) {
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
    }

    public String parseJwt(HttpServletRequest request) {
        String headerAuth = request.getHeader("Authorization");

        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
            return headerAuth.substring(7, headerAuth.length());
        }

        return null;
    }

    public boolean validateJwtToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            logger.error("Invalid JWT signature: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            logger.error("JWT token is expired: {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            logger.error("JWT token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            logger.error("JWT claims string is empty: {}", e.getMessage());
        }
        return false;
    }

    public boolean isBlacklisted(String authToken) {
        try{
            return blacklistTokenRepository.findByToken(authToken);
        }catch (Exception e){
            logger.error("JWT blacklist check exception: {}", e.getMessage());
            return false;
        }
    }

    public boolean isWhitelisted(String authToken) {
        try{
            return whitelistTokenRepository.findByToken(authToken);
        }catch (Exception e){
            logger.error("JWT whitelist check exception: {}", e.getMessage());
            return false;
        }
    }
}
