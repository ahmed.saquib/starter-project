package net.spectrum.api.security.jwt.responseentity;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.spectrum.api.security.menu.entity.MenuEntity;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class JwtResponseEntity {
    private String token;
    private Long id;
    private String username;
    private String email;
    private List<String> roles;

    private List<MenuEntity> menuJson;
}
