package net.spectrum.api.security.jwt.blacklisttoken.entity;


import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@RedisHash("BlacklistToken")
public class BlacklistTokenEntity implements Serializable{
    @Id
    private String token;
}
