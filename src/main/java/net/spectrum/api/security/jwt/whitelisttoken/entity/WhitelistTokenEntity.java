package net.spectrum.api.security.jwt.whitelisttoken.entity;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@RedisHash("WhitelistToken")
public class WhitelistTokenEntity implements Serializable {
    @Id
    private String token;
}
