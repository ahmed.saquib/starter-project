package net.spectrum.api.security.auth.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import net.spectrum.api.security.jwt.blacklisttoken.entity.BlacklistTokenEntity;
import net.spectrum.api.security.jwt.blacklisttoken.repository.BlacklistTokenRepository;
import net.spectrum.api.security.jwt.responseentity.JwtResponseEntity;
import net.spectrum.api.security.jwt.utils.JwtUtils;
import net.spectrum.api.security.auth.loginrequest.entity.LoginRequestEntity;
import net.spectrum.api.security.auth.signuprequest.entity.SignupRequestEntity;
import net.spectrum.api.security.jwt.whitelisttoken.entity.WhitelistTokenEntity;
import net.spectrum.api.security.jwt.whitelisttoken.repository.WhitelistTokenRepository;
import net.spectrum.api.security.menu.entity.MenuEntity;
import net.spectrum.api.security.menu.repository.MenuRepository;
import net.spectrum.api.security.user.entity.UserEntity;
import net.spectrum.api.security.userdetails.entity.UserDetailsImpl;
import net.spectrum.api.security.user.repository.UserRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin()
@RestController
@RequestMapping("/v1/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    BlacklistTokenRepository blacklistTokenRepository;

    @Autowired
    WhitelistTokenRepository whitelistTokenRepository;

    @Autowired
    MenuRepository menuRepository;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    PasswordEncoder passwordEncoder;

    DaoAuthenticationProvider daoAuthenticationProvider;

    @PostMapping("/sign-in")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequestEntity loginRequest) {
        if (!loginRequest.getUsername().isEmpty()) {
            if (userRepository.findByUsername(loginRequest.getUsername()).isPresent()) {
                    Authentication authentication = authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),
                            loginRequest.getPassword()));
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                    String token = jwtUtils.generateJwtToken(authentication);
                    whitelistTokenRepository.save(new WhitelistTokenEntity(token));
                    UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
                    List<String> roles = userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority)
                        .collect(Collectors.toList());
                     Optional<UserEntity> userEntity = userRepository.findByUsername(loginRequest.getUsername());
                    return ResponseEntity.ok(
                        new JwtResponseEntity(token, userDetails.getId(), userDetails.getUsername(),
                            userDetails.getEmail(), roles, userEntity.get().getMenu()));
            } else {
                return ResponseEntity.status(400).body("Username not found.");
            }
        } else {
            return ResponseEntity.status(400).body("Username can't be null.");
        }
    }

    @PostMapping("/sign-up")
    public ResponseEntity<String> registerUser(@Valid @RequestBody @NotNull SignupRequestEntity signUpRequest) {
        if (Boolean.TRUE.equals(userRepository.existsByUsername(signUpRequest.getUsername()))) {
            return ResponseEntity.badRequest().body("Error: Username is already taken!");
        }

        if (Boolean.TRUE.equals(userRepository.existsByEmail(signUpRequest.getEmail()))) {
            return ResponseEntity.badRequest().body("Error: Email is already in use!");
        }

        UserEntity userEntity = new UserEntity(signUpRequest.getUsername(), signUpRequest.getEmail(),
            passwordEncoder.encode(signUpRequest.getPassword()));

        List<String> requestedMenus = signUpRequest.getAccessMenus();
        List<MenuEntity> userAccessMenus = new ArrayList<>();

        if (requestedMenus == null) {
            userEntity.setMenu(null);
        } else {
            requestedMenus.forEach(menu -> {
                MenuEntity menuEntity = menuRepository.findByOid(menu)
                    .orElseThrow(() -> new RuntimeException("Error: Menu not found."));
                userAccessMenus.add(menuEntity);
            });
        }
        userEntity.setMenu(userAccessMenus);
        userRepository.save(userEntity);

        return ResponseEntity.ok(String.format("User %s registered successfully!", userEntity.getUsername()));
    }

    @PostMapping("/sign-out")
    public ResponseEntity<String> logoutUser(HttpServletRequest request) {
        BlacklistTokenEntity savedToken = blacklistTokenRepository.save(
            new BlacklistTokenEntity(jwtUtils.parseJwt(request)));
        if (savedToken != null) {
            return ResponseEntity.ok("You've been signed out!");
        } else {
            return ResponseEntity.status(500).body("Error processing request.");
        }
    }


    @PreAuthorize("hasAuthority('createUserProfile')") // alternative: @PreAuthorize("hasRole('TESTER')"), @Secured("TESTER")
    @GetMapping("/hello")
    public ResponseEntity<String> getHello() {
        return ResponseEntity.ok("Hello " + SecurityContextHolder.getContext().getAuthentication().getName());
    }
}
