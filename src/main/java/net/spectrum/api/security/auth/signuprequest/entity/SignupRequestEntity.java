package net.spectrum.api.security.auth.signuprequest.entity;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SignupRequestEntity {

    private String username;

    private String email;

    private String password;

    private List<String> accessMenus;
}
