package net.spectrum.api.security.auth.requestfilter;
import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.spectrum.api.security.jwt.blacklisttoken.repository.BlacklistTokenRepository;
import net.spectrum.api.security.jwt.utils.JwtUtils;
import net.spectrum.api.security.userdetails.service.impl.UserDetailsServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

@Configuration
public class AuthTokenFilter extends OncePerRequestFilter {
    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private BlacklistTokenRepository blacklistTokenRepository;

    private static final Logger log = LoggerFactory.getLogger(AuthTokenFilter.class);

//  What we do inside doFilterInternal():
//    – get JWT from the HTTP Cookies
//    – if the request has JWT, validate it, parse username from it
//    – from username, get UserDetails to create an Authentication object
//    – set the current UserDetails in SecurityContext using setAuthentication(authentication) method.

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
        throws ServletException, IOException {
        try {
            String jwt = jwtUtils.parseJwt(request);
            if (jwt != null && jwtUtils.isWhitelisted(jwt) && !jwtUtils.isBlacklisted(jwt)) {
                if (jwtUtils.validateJwtToken(jwt)){
                    String username = jwtUtils.getUserNameFromJwtToken(jwt);
                    UserDetails userDetails = userDetailsService.loadUserByUsername(username);
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities());
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
        } catch (Exception e) {
            log.error("Cannot set user authentication: $s", e);
        }
        filterChain.doFilter(request, response);
    }
}
