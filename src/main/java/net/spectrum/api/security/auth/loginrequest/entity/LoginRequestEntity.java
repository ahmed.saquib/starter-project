package net.spectrum.api.security.auth.loginrequest.entity;

import lombok.Getter;

@Getter
public class LoginRequestEntity {
    String username;
    String password;
}
