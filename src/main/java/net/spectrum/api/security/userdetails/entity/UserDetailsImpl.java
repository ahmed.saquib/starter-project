package net.spectrum.api.security.userdetails.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import net.spectrum.api.security.menu.entity.MenuEntity;
import net.spectrum.api.security.user.entity.UserEntity;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class UserDetailsImpl implements UserDetails {

    private static final long serialVersionUID = 1L;
    private final Long id;

    private final String username;

    private final String email;

    private final List<MenuEntity> menuEntities;

    @JsonIgnore
    private final String password;

    private final Collection<? extends GrantedAuthority> authorities;

    public UserDetailsImpl(Long id, String username, String email, List<MenuEntity> menuEntities, String password,
        Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.authorities = authorities;
        this.menuEntities = menuEntities;
    }

    public static UserDetailsImpl build(UserEntity userEntity) {
        List<String> userPrivileges = new ArrayList<>();
        List<MenuEntity> menuEntities = userEntity.getMenu();
        for(MenuEntity menuEntity: menuEntities){
            JSONObject menuJson = new JSONObject(menuEntity.getMenuJson());
            userPrivileges = getPrivileges(menuJson);
        }
        List<GrantedAuthority> authorities = userPrivileges.stream()
            .map(SimpleGrantedAuthority::new) // .map(privilege -> new SimpleGrantedAuthority(privilege))
            .collect(Collectors.toList());

        return new UserDetailsImpl(
            userEntity.getId(),
            userEntity.getUsername(),
            userEntity.getEmail(),
            menuEntities,
            userEntity.getPassword(),
            authorities);
    }

    private static List<String> getPrivileges(JSONObject jsonObject){
        List<String> userPrivileges = new ArrayList<>();
        JSONArray privilegeList = jsonObject.getJSONArray("privileges");
        for (Object privilege : privilegeList){
            userPrivileges.add(privilege.toString());
        }
        JSONArray childList = jsonObject.getJSONArray("childs");
        if(!childList.isEmpty()){
            for (Object child : childList){
                List<String>  childPrivileges =  getPrivileges((JSONObject) child);
                userPrivileges.addAll(childPrivileges);
            }
        }
        return userPrivileges;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        UserDetailsImpl user = (UserDetailsImpl) o;
        return Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
