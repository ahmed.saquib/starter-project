package net.spectrum.api.util.objconverter;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ObjectConverter<S, D> {

    ModelMapper modelMapper = new ModelMapper();

    public Object convert(S source, D destination){
        return modelMapper.map(source, destination.getClass());
    }
}
