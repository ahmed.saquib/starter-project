package net.spectrum.api.config.aop.handleexception;

import net.spectrum.api.config.aop.handleaspect.AspectHandler;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestRequestExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected @NotNull ResponseEntity<Object> handleHttpMessageNotReadable(@NotNull HttpMessageNotReadableException exception, @NotNull HttpHeaders headers, @NotNull HttpStatus status, @NotNull WebRequest request) {
        AspectHandler.setException(exception);
        AspectHandler.setIsExceptionThrown(true);
        return ExceptionHandler.handleException(exception);
    }
}
