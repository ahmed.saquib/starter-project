package net.spectrum.api.config.aop.handleaspect;

import java.time.Instant;
import javax.servlet.http.HttpServletRequest;
import net.spectrum.api.config.aop.handleexception.ExceptionHandler;
import net.spectrum.api.config.aop.handlelog.LogHandler;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Configuration
public class AspectHandler {

    private  static Throwable exception;
    private Instant processStartTime;
    private Instant processEndTime;
    private LogHandler logHandler;
    private static Object proceedingJoinPoint;
    private  static boolean isExceptionThrown = false;

    public static void setIsExceptionThrown(boolean isExceptionThrown) {
        AspectHandler.isExceptionThrown = isExceptionThrown;
    }

    public  void initializeLogHandler(ProceedingJoinPoint joinPoint) {
        logHandler = new LogHandler(getRequest(), joinPoint);
    }

    public static void setException(Throwable exception) {
        AspectHandler.exception = exception;
    }
    public  boolean isExceptionThrown() {
        return isExceptionThrown;
    }

    public Throwable getException() {
        return exception;
    }

    public  Object getProceedingJoinPoint() {
        return proceedingJoinPoint;
    }

    protected HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
    }

    public void handleAfterExecutionLog() {
        logHandler.afterExecutionLog(processEndTime, processStartTime);
        resetExceptionState();
    }

    public static void resetExceptionState(){
        exception = null;
        isExceptionThrown = false;
    }

    public void handleExceptionLog() {
            logHandler.exceptionLog(exception);
    }

    public  void handleBeforeExecutionLog() {
        logHandler.beforeExecutionLog(processStartTime);
    }

    public void handleProceedResonseLog() {
        logHandler.proceedResponseLog(proceedingJoinPoint);
    }

    public  void saveProcessStartTime() {
        processStartTime = Instant.now();
    }

    public static void proceedJoinPoint(ProceedingJoinPoint joinPoint) {
        try {
            proceedingJoinPoint = joinPoint.proceed();
        } catch (Throwable e) {
            isExceptionThrown = true;
            exception = e;
            proceedingJoinPoint = ExceptionHandler.handleException(e);
        }
    }

    public static void setProceedingJoinPoint(Object proceedingJoinPoint) {
        AspectHandler.proceedingJoinPoint = proceedingJoinPoint;
    }
    public void saveProcessEndTime() {
        processEndTime = Instant.now();
    }
}
