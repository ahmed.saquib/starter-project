package net.spectrum.api.config.aop.handleexception.messagebuilder;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.converter.HttpMessageNotReadableException;

public class ExceptionMessageBuilder {

    private ExceptionMessageBuilder(){}
    public static String buildExceptionMessage(Throwable e) {
        String exceptionMessage = null;
        if(e instanceof DataIntegrityViolationException) {
            exceptionMessage = e.getCause().getMessage();
            String[] arrOfExceptionMessage = exceptionMessage.split(":", 2);
            String[] arrExceptionMessage = arrOfExceptionMessage[1].split("\\.");
            String exceptionFor = arrExceptionMessage[arrExceptionMessage.length - 1];
            exceptionMessage = exceptionFor + " should not be null";
        } else if (e instanceof HttpMessageNotReadableException) {
            exceptionMessage = e.getMessage();
            String[] arrOfExceptionMessage = exceptionMessage.split(":", 5);
            String[] arrIndexFourMessage = arrOfExceptionMessage[4].split("\\[");
            exceptionMessage = arrIndexFourMessage[0];
            String exceptionfor = arrIndexFourMessage[arrIndexFourMessage.length-1].
                    replaceAll("[\\[]|[\\)]|[\\]]|[\\(]|[\"]",
                            "");
            exceptionMessage = exceptionMessage+exceptionfor;
        }
        return exceptionMessage;
    }
}
