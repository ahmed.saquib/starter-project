package net.spectrum.api.config.aop.handlelog;

import java.time.Duration;
import java.time.Instant;
import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;

public class LogHandler {
    private final String requestUrl;
    private final String requestMethod;
    private final String serverName;
    private final String serverPort;
    private final Logger logger;
    private final String methodName;
    private final String className;
    private final String pathFromSource;

    public LogHandler(HttpServletRequest request, ProceedingJoinPoint joinPoint) {
        this.requestUrl = String.valueOf(request.getRequestURL());
        this.requestMethod = request.getMethod();
        this.serverName = request.getServerName();
        this.serverPort = String.valueOf(request.getServerPort());
        this.logger = LoggerFactory.getLogger(joinPoint.getSignature().getDeclaringTypeName());
        this.methodName = String.valueOf(joinPoint.getSignature().getName()); //
        this.className = joinPoint.getSignature().getDeclaringType().getSimpleName();
        this.pathFromSource = joinPoint.getSignature().getDeclaringTypeName();
    }

    public void beforeExecutionLog(Instant startTime) {
        logger.debug("Request from: {}:{}", serverName, serverPort);
        logger.debug("RequestUrl: {}, Method: {}", requestUrl, requestMethod);
        logger.debug("Target: {}.{}()", pathFromSource, methodName);
        logger.debug("Execute: {}.{}(), start time: {}", className, methodName, startTime);
    }

    public void proceedResponseLog(Object proceedResponse) {
        logger.debug("Got result from: {}()", methodName);
        logger.debug("Result: {}", proceedResponse);
    }

    public void afterExecutionLog(Instant endTime, Instant processStartTime) {
        logger.debug("Exit: {}.{}(), exit time: {}", className, methodName, endTime);
        logger.debug("RequestUrl: {}, Process time: {}", requestUrl, Duration.between(processStartTime, endTime));
    }

    public void exceptionLog(Throwable exception) {
         logger.debug("Exception found while trying to execute: {}.{}()", className, methodName);
        if (exception instanceof DataIntegrityViolationException) {
            logger.debug("Exception: {}, Message: {}", exception.getClass().getSimpleName(), exception.getCause().getMessage());
        } else {
            logger.debug("Exception: {}, Message: {}", exception.getClass().getSimpleName(), exception.getMessage());
        }
    }

}
