package net.spectrum.api.config.aop;

import net.spectrum.api.config.aop.handleaspect.AspectHandler;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Pointcut;

import org.springframework.stereotype.Component;

@org.aspectj.lang.annotation.Aspect
@Component
public class Aspect extends AspectHandler {

    @Pointcut("within(*..*RestRequestExceptionHandler) || within(net.spectrum.api..*Controller)")
    public void endpointHandlerPointcut() {
    }

    @Around("endpointHandlerPointcut()")
    public Object handlePointCut(ProceedingJoinPoint joinPoint) {
        saveProcessStartTime();
        initializeLogHandler(joinPoint);
        handleBeforeExecutionLog();
        proceedJoinPoint(joinPoint);
        handleProceedResonseLog();
        if(isExceptionThrown()){
            handleExceptionLog();
        }
        saveProcessEndTime();
        handleAfterExecutionLog();
        return getProceedingJoinPoint();
    }
}