package net.spectrum.api.config.aop.handleexception;

import net.spectrum.api.config.aop.handleaspect.AspectHandler;
import net.spectrum.api.config.aop.handleexception.messagebuilder.ExceptionMessageBuilder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;

public class ExceptionHandler {

    private ExceptionHandler() {}

    public static ResponseEntity<Object> handleException(Throwable e) {
        if (e instanceof DataIntegrityViolationException) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(ExceptionMessageBuilder.buildExceptionMessage(e));
        } else if (e instanceof HttpMessageNotReadableException) {
            ResponseEntity<Object> object = ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(ExceptionMessageBuilder.buildExceptionMessage(e));
            AspectHandler.setProceedingJoinPoint(object);
            return object;
        } else if (e instanceof RuntimeException) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        } else {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }
    }
}
